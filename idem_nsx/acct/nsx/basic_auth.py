import base64
from typing import Any
from typing import Dict


DEFAULT_ENDPOINT_URL = "https://nsxmanager.your.domain/policy/api/v1"


async def gather(hub, profiles) -> Dict[str, Any]:
    """
    Generate token with basic auth

    Example:
    .. code-block:: yaml

        nsx:
          profile_name:
            username: my_user
            password: my_token
            endpoint_url: https://nsxmanager.your.domain/policy/api/v1
    """
    sub_profiles = {}
    for (
        profile,
        ctx,
    ) in profiles.get("nsx", {}).items():
        endpoint_url = ctx.get("endpoint_url")

        creds = f"{ctx.get('username')}:{ctx.get('password')}"
        sub_profiles[profile] = dict(
            endpoint_url=endpoint_url,
            headers={
                "Authorization": f"Basic {base64.b64encode(creds.encode('utf-8')).decode('ascii')}"
            },
        )
    return sub_profiles
