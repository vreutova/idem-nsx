"""States module for managing Policy Networking Connectivity Segments Port. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    segment_id: str,
    resource_id: str = None,
    address_bindings: List[
        make_dataclass(
            "address_bindings",
            [
                ("ip_address", str, field(default=None)),
                ("mac_address", str, field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ] = None,
    admin_state: str = None,
    attachment: make_dataclass(
        "attachment",
        [
            ("allocate_addresses", str, field(default=None)),
            ("app_id", str, field(default=None)),
            (
                "bms_interface_config",
                make_dataclass(
                    "bms_interface_config",
                    [
                        ("app_intf_name", str),
                        ("default_gateway", str, field(default=None)),
                        ("migrate_intf", str, field(default=None)),
                        ("routing_table", List[str], field(default=None)),
                    ],
                ),
                field(default=None),
            ),
            ("context_id", str, field(default=None)),
            ("context_type", str, field(default=None)),
            ("evpn_vlans", List[str], field(default=None)),
            ("hyperbus_mode", str, field(default=None)),
            ("id", str, field(default=None)),
            ("traffic_tag", int, field(default=None)),
            ("type", str, field(default=None)),
        ],
    ) = None,
    extra_configs: List[
        make_dataclass(
            "extra_configs",
            [
                (
                    "config_pair",
                    make_dataclass("config_pair", [("key", str), ("value", str)]),
                )
            ],
        )
    ] = None,
    ignored_address_bindings: List[
        make_dataclass(
            "ignored_address_bindings",
            [
                ("ip_address", str, field(default=None)),
                ("mac_address", str, field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ] = None,
    init_state: str = None,
    origin_id: str = None,
    source_site_id: str = None,
    children: List[
        make_dataclass(
            "children",
            [
                ("mark_for_override", bool, field(default=None)),
                ("marked_for_delete", bool, field(default=None)),
                (
                    "request_parameter",
                    make_dataclass("request_parameter", [("resource_type", str)]),
                    field(default=None),
                ),
                ("resource_type", str, field(default=None)),
                ("_create_time", int, field(default=None)),
                ("_create_user", str, field(default=None)),
                ("_last_modified_time", int, field(default=None)),
                ("_last_modified_user", str, field(default=None)),
                ("_protection", str, field(default=None)),
                ("_system_owned", bool, field(default=None)),
                ("description", str, field(default=None)),
                ("display_name", str, field(default=None)),
                ("id", str, field(default=None)),
                (
                    "tags",
                    List[
                        make_dataclass(
                            "tags",
                            [
                                ("scope", str, field(default=None)),
                                ("tag", str, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ] = None,
    marked_for_delete: bool = None,
    overridden: bool = None,
    origin_site_id: str = None,
    owner_id: str = None,
    parent_path: str = None,
    path: str = None,
    realization_id: str = None,
    relative_path: str = None,
    remote_path: str = None,
    unique_id: str = None,
) -> Dict[str, Any]:
    """
        Create an infra segment port if it does not exist based on the IDs, or update existing port
    information by replacing the port object fields which presents in the request body.

    Args:
        name(str):
            Idem name of the resource.

        segment_id(str):
            Segment unique ID.

        resource_id(str, Optional):
            Port unique ID. Defaults to None.

        address_bindings(List[dict[str, Any]], Optional):
            Static address binding used for the port. Defaults to None.

            * ip_address (str, Optional):
                IP Address for port binding. Defaults to None.

            * mac_address (str, Optional):
                Mac address for port binding. Defaults to None.

            * vlan_id (int, Optional):
                VLAN ID for port binding. Defaults to None.

        admin_state(str, Optional):
            Represents desired state of the segment port. Defaults to None.

        attachment(dict[str, Any], Optional):
            attachment. Defaults to None.

            * allocate_addresses (str, Optional):
                Indicate how IP will be allocated for the port. Defaults to None.

            * app_id (str, Optional):
                ID used to identify/look up a child attachment behind a parent attachment
                Defaults to None.

            * bms_interface_config (dict[str, Any], Optional):
                bms_interface_config. Defaults to None.

                * app_intf_name (str):
                    The name of application interface.

                * default_gateway (str, Optional):
                    Gateway IP. Defaults to None.

                * migrate_intf (str, Optional):
                    IP configuration on migrate_intf will migrate to app_intf_name. It is used for Management and Application sharing the same IP. Defaults to None.

                * routing_table (List[str], Optional):
                    Routing rules. Defaults to None.

            * context_id (str, Optional):
                If type is CHILD and the parent port is on the same segment as the child port, then this
                field should be VIF ID of the parent port.
                If type is CHILD and the parent port is on a different segment, then this
                field should be policy path of the parent port.
                If type is INDEPENDENT/STATIC, then this field should be transport node ID.
                Defaults to None.

            * context_type (str, Optional):
                Set to PARENT when type field is CHILD. Read only field. Defaults to None.

            * evpn_vlans (List[str], Optional):
                List of Evpn tenant VLAN IDs the Parent logical-port serves in Evpn Route-Server mode. Only effective when attachment type is PARENT and the logical-port is attached to vRouter VM. Defaults to None.

            * hyperbus_mode (str, Optional):
                Flag to indicate if hyperbus configuration is required. Defaults to None.

            * id (str, Optional):
                VIF UUID on NSX Manager. If the attachement type is PARENT, this property is required. Defaults to None.

            * traffic_tag (int, Optional):
                Not valid when type field is INDEPENDENT, mainly used to identify
                traffic from different port in container use case.
                Defaults to None.

            * type (str, Optional):
                Type of port attachment. STATIC is added to replace INDEPENDENT. INDEPENDENT type and PARENT type are deprecated. Defaults to None.

        extra_configs(List[dict[str, Any]], Optional):
            This property could be used for vendor specific configuration in key value
            string pairs. Segment port setting will override segment setting if
            the same key was set on both segment and segment port.
            Defaults to None.

            * config_pair (dict[str, Any]):
                config_pair.

                * key (str):
                    Key.

                * value (str):
                    Value.

        ignored_address_bindings(List[dict[str, Any]], Optional):
            IP Discovery module uses various mechanisms to discover address
            bindings being used on each segment port. If a user would like to
            ignore any specific discovered address bindings or prevent the
            discovery of a particular set of discovered bindings, then those
            address bindings can be provided here. Currently IP range in CIDR format
            is not supported.
            Defaults to None.

            * ip_address (str, Optional):
                IP Address for port binding. Defaults to None.

            * mac_address (str, Optional):
                Mac address for port binding. Defaults to None.

            * vlan_id (int, Optional):
                VLAN ID for port binding. Defaults to None.

        init_state(str, Optional):
            Set initial state when a new logical port is created. 'UNBLOCKED_VLAN'
            means new port will be unblocked on traffic in creation, also VLAN will
            be set with corresponding logical switch setting. This port setting
            can only be configured at port creation, and cannot be modified.
            'RESTORE_VIF' fetches and restores VIF attachment from ESX host.
            Defaults to None.

        origin_id(str, Optional):
            ID populated by NSX when NSX on DVPG is used to indicate the source Distributed Virtual Port
            and the corresponding Distributed Virtual Switch. This ID is populated only for port attached to
            discovered segments.
            Defaults to None.

        source_site_id(str, Optional):
            This field will refer to the source site on which the segment
            port is discovered. This field is populated by GM, when it
            receives corresponding notification from LM.
            Defaults to None.

        children(List[dict[str, Any]], Optional):
            Subtree for this type within policy tree containing nested elements. Note that
            this type is applicable to be used in Hierarchical API only.
            Defaults to None.

            * mark_for_override (bool, Optional):
                Indicates whether this object is the overridden intent object Global intent objects cannot be modified by the user. However, certain global intent objects can be overridden locally by use of this property. In such cases, the overridden local values take precedence over the globally defined values for the properties. Defaults to None.

            * marked_for_delete (bool, Optional):
                If this field is set to true, delete operation is triggered on the
                intent tree. This resource along with its all children in intent tree
                will be deleted. This is a cascade delete and should only be used if
                intent object along with its all children are to be deleted. This does
                not support deletion of single non-leaf node within the tree and should
                be used carefully.
                Defaults to None.

            * request_parameter (dict[str, Any], Optional):
                request_parameter. Defaults to None.

                * resource_type (str):
                    The type of this request parameter.

            * resource_type (str, Optional):
                The type of this resource. Defaults to None.

            * _create_time (int, Optional):
                Timestamp of resource creation. Defaults to None.

            * _create_user (str, Optional):
                ID of the user who created this resource. Defaults to None.

            * _last_modified_time (int, Optional):
                Timestamp of last modification. Defaults to None.

            * _last_modified_user (str, Optional):
                ID of the user who last modified this resource. Defaults to None.

            * _protection (str, Optional):
                Protection status is one of the following:
                PROTECTED - the client who retrieved the entity is not allowed
                            to modify it.
                NOT_PROTECTED - the client who retrieved the entity is allowed
                                to modify it
                REQUIRE_OVERRIDE - the client who retrieved the entity is a super
                                   user and can modify it, but only when providing
                                   the request header X-Allow-Overwrite=true.
                UNKNOWN - the _protection field could not be determined for this
                          entity.
                Defaults to None.

            * _system_owned (bool, Optional):
                Indicates system owned resource. Defaults to None.

            * description (str, Optional):
                Description of this resource. Defaults to None.

            * display_name (str, Optional):
                Defaults to ID if not set. Defaults to None.

            * id (str, Optional):
                Unique identifier of this resource. Defaults to None.

            * tags (List[dict[str, Any]], Optional):
                Opaque identifiers meaningful to the API user. Defaults to None.

                * scope (str, Optional):
                    Tag searches may optionally be restricted by scope. Defaults to None.

                * tag (str, Optional):
                    Identifier meaningful to user with maximum length of 256 characters. Defaults to None.

        marked_for_delete(bool, Optional):
            Intent objects are not directly deleted from the system when a delete
            is invoked on them. They are marked for deletion and only when all the
            realized entities for that intent object gets deleted, the intent object
            is deleted. Objects that are marked for deletion are not returned in
            GET call. One can use the search API to get these objects.
            Defaults to None.

        overridden(bool, Optional):
            Global intent objects cannot be modified by the user.
            However, certain global intent objects can be overridden locally by use
            of this property. In such cases, the overridden local values take
            precedence over the globally defined values for the properties.
            Defaults to None.

        origin_site_id(str, Optional):
            This is a UUID generated by the system for knowing which site owns an object.
            This is used in Pmaas
            Defaults to None.

        owner_id(str, Optional):
            This is a UUID generated by the system for knowing whoes owns this object.
            This is used in Pmaas
            Defaults to None.

        parent_path(str, Optional):
            Path of its parent. Defaults to None.

        path(str, Optional):
            Absolute path of this object. Defaults to None.

        realization_id(str, Optional):
            This is a UUID generated by the system for realizing the entity object.
            In most cases this should be same as 'unique_id' of the entity. However,
            in some cases this can be different because of entities have migrated thier
            unique identifier to NSX Policy intent objects later in the timeline and did
            not use unique_id for realization. Realization id is helpful for users to
            debug data path to correlate the configuration with corresponding intent.
            Defaults to None.

        relative_path(str, Optional):
            Path relative from its parent. Defaults to None.

        remote_path(str, Optional):
            This is the path of the object on the local managers when queried on the PMaaS service,
            and path of the object on PMaaS service when queried from the local managers.
            Defaults to None.

        unique_id(str, Optional):
            This is a UUID generated by the GM/LM to uniquely identify
            entites in a federated environment. For entities that are
            stretched across multiple sites, the same ID will be used
            on all the stretched sites.
            Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_nsx.policy.networking.connectivity.segments.port_is_present:
              nsx.nsx.policy.networking.connectivity.segments.port.present:
              - segment_id: string
              - port_id: string
              - address_bindings:
                - ip_address: string
                  mac_address: string
                  vlan_id: int
              - admin_state: string
              - attachment:
                  allocate_addresses: string
                  app_id: string
                  bms_interface_config:
                    app_intf_name: string
                    default_gateway: string
                    migrate_intf: string
                    routing_table:
                    - value
                  context_id: string
                  context_type: string
                  evpn_vlans:
                  - value
                  hyperbus_mode: string
                  id_: string
                  traffic_tag: int
                  type_: string
              - extra_configs:
                - config_pair:
                    key: string
                    value: string
              - ignored_address_bindings:
                - ip_address: string
                  mac_address: string
                  vlan_id: int
              - init_state: string
              - origin_id: string
              - source_site_id: string
              - children:
                - _create_time: int
                  _create_user: string
                  _last_modified_time: int
                  _last_modified_user: string
                  _protection: string
                  _system_owned: bool
                  description: string
                  display_name: string
                  id_: string
                  mark_for_override: bool
                  marked_for_delete: bool
                  request_parameter:
                    resource_type: string
                  resource_type: string
                  tags:
                  - scope: string
                    tag: string
              - marked_for_delete: bool
              - overridden: bool
              - origin_site_id: string
              - owner_id: string
              - parent_path: string
              - path: string
              - realization_id: string
              - relative_path: string
              - remote_path: string
              - unique_id: string
    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        before = await hub.exec.nsx.policy.networking.connectivity.segments.port.get(
            ctx,
            name=name,
            segment_id=segment_id,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'nsx.port: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.nsx.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update nsx.port: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.nsx.policy.networking.connectivity.segments.port.update(
                    ctx,
                    segment_id=segment_id,
                    **{
                        "_revision": before["ret"]["_revision"],
                        "resource_id": resource_id,
                        "name": name,
                        "address_bindings": address_bindings,
                        "admin_state": admin_state,
                        "attachment": attachment,
                        "extra_configs": extra_configs,
                        "ignored_address_bindings": ignored_address_bindings,
                        "init_state": init_state,
                        "origin_id": origin_id,
                        "source_site_id": source_site_id,
                        "children": children,
                        "marked_for_delete": marked_for_delete,
                        "overridden": overridden,
                        "origin_site_id": origin_site_id,
                        "owner_id": owner_id,
                        "parent_path": parent_path,
                        "path": path,
                        "realization_id": realization_id,
                        "relative_path": relative_path,
                        "remote_path": remote_path,
                        "unique_id": unique_id,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'nsx.port: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.nsx.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create nsx.port: {name}",)
            return result
        else:
            create_ret = (
                await hub.exec.nsx.policy.networking.connectivity.segments.port.create(
                    ctx,
                    segment_id=segment_id,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "address_bindings": address_bindings,
                        "admin_state": admin_state,
                        "attachment": attachment,
                        "extra_configs": extra_configs,
                        "ignored_address_bindings": ignored_address_bindings,
                        "init_state": init_state,
                        "origin_id": origin_id,
                        "source_site_id": source_site_id,
                        "children": children,
                        "marked_for_delete": marked_for_delete,
                        "overridden": overridden,
                        "origin_site_id": origin_site_id,
                        "owner_id": owner_id,
                        "parent_path": parent_path,
                        "path": path,
                        "realization_id": realization_id,
                        "relative_path": relative_path,
                        "remote_path": remote_path,
                        "unique_id": unique_id,
                    },
                )
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'nsx.port: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    after = await hub.exec.nsx.policy.networking.connectivity.segments.port.get(
        ctx,
        name=name,
        segment_id=segment_id,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub, ctx, name: str, resource_id: str = None, segment_id: str = None
) -> Dict[str, Any]:
    """

    Delete an infra segment port
        Delete an infra segment port by giving ID.

    Args:
        name(str):
            Idem name of the resource.

        segment_id(str):
            Segment unique ID. Defaults to None.

        resource_id(str, Optional):
            Port unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_nsx.policy.networking.connectivity.segments.port_is_absent:
              nsx.nsx.policy.networking.connectivity.segments.port.absent:
              - segment_id: string
              - resource_id: string
    """
    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'nsx.port: {name}' already absent")
        return result

    before = await hub.exec.nsx.policy.networking.connectivity.segments.port.get(
        ctx, name=name, segment_id=segment_id, resource_id=resource_id
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete nsx.port: {name}"
            return result

        delete_ret = (
            await hub.exec.nsx.policy.networking.connectivity.segments.port.delete(
                ctx, name=name, resource_id=resource_id, segment_id=segment_id
            )
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted nsx.port: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"nsx.port: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function

    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe nsx.policy.networking.connectivity.segments.port
    """

    result = {}

    segments_ret = (
        await hub.exec.nsx.policy.networking.connectivity.segments.segment.list(ctx)
    )
    if not segments_ret["result"]:
        hub.log.debug(f"Could not describe segments {segments_ret['comment']}")
        result["comment"] = segments_ret["comment"]
        result["result"] = False
        return result

    for segment in segments_ret["ret"]:
        segment_id = segment.get("resource_id")
        ports_ret = (
            await hub.exec.nsx.policy.networking.connectivity.segments.port.list(
                ctx, segment_id
            )
        )

        if not ports_ret["result"]:
            hub.log.debug(
                f"Could not describe segment '{segment_id}' ports {segments_ret['comment']}"
            )
            continue

        for port in ports_ret["ret"]:
            resource_id = port.get("resource_id")
            result[resource_id] = {
                "nsx.policy.networking.connectivity.segments.port.present": [
                    {parameter_key: parameter_value}
                    for parameter_key, parameter_value in port.items()
                ]
            }
    return result
