"""States module for managing Policy Networking Connectivity Segment. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    address_bindings: List[
        make_dataclass(
            "address_bindings",
            [
                ("ip_address", str, field(default=None)),
                ("mac_address", str, field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ] = None,
    admin_state: str = None,
    advanced_config: make_dataclass(
        "advanced_config",
        [
            ("address_pool_paths", List[str], field(default=None)),
            ("hybrid", bool, field(default=None)),
            ("inter_router", bool, field(default=None)),
            ("local_egress", bool, field(default=None)),
            (
                "local_egress_routing_policies",
                List[
                    make_dataclass(
                        "local_egress_routing_policies",
                        [("nexthop_address", str), ("prefix_list_paths", List[str])],
                    )
                ],
                field(default=None),
            ),
            ("multicast", bool, field(default=None)),
            ("ndra_profile_path", str, field(default=None)),
            ("node_local_switch", bool, field(default=None)),
            ("origin_id", str, field(default=None)),
            ("origin_type", str, field(default=None)),
            ("uplink_teaming_policy_name", str, field(default=None)),
            ("urpf_mode", str, field(default=None)),
            ("connectivity", str, field(default=None)),
        ],
    ) = None,
    bridge_profiles: List[
        make_dataclass(
            "bridge_profiles",
            [
                ("bridge_profile_path", str),
                ("vlan_transport_zone_path", str),
                ("uplink_teaming_policy_name", str, field(default=None)),
                ("vlan_ids", List[str], field(default=None)),
            ],
        )
    ] = None,
    connectivity_path: str = None,
    dhcp_config_path: str = None,
    domain_name: str = None,
    evpn_segment: bool = None,
    evpn_tenant_config_path: str = None,
    extra_configs: List[
        make_dataclass(
            "extra_configs",
            [
                (
                    "config_pair",
                    make_dataclass("config_pair", [("key", str), ("value", str)]),
                )
            ],
        )
    ] = None,
    federation_config: make_dataclass(
        "federation_config", [("global_overlay_id", int, field(default=None))]
    ) = None,
    l2_extension: make_dataclass(
        "l2_extension",
        [
            ("l2vpn_path", str, field(default=None)),
            ("l2vpn_paths", List[str], field(default=None)),
            (
                "local_egress",
                make_dataclass(
                    "local_egress", [("optimized_ips", List[str], field(default=None))]
                ),
                field(default=None),
            ),
            ("tunnel_id", int, field(default=None)),
        ],
    ) = None,
    ls_id: str = None,
    mac_pool_id: str = None,
    metadata_proxy_paths: List[str] = None,
    overlay_id: int = None,
    replication_mode: str = None,
    subnets: List[
        make_dataclass(
            "subnets",
            [
                (
                    "dhcp_config",
                    make_dataclass(
                        "dhcp_config",
                        [
                            ("resource_type", str),
                            ("dns_servers", List[str], field(default=None)),
                            ("lease_time", int, field(default=None)),
                            ("server_address", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("dhcp_ranges", List[str], field(default=None)),
                ("gateway_address", str, field(default=None)),
                ("network", str, field(default=None)),
            ],
        )
    ] = None,
    transport_zone_path: str = None,
    type_: str = None,
    vlan_ids: List[str] = None,
    children: List[
        make_dataclass(
            "children",
            [
                ("mark_for_override", bool, field(default=None)),
                ("marked_for_delete", bool, field(default=None)),
                (
                    "request_parameter",
                    make_dataclass("request_parameter", [("resource_type", str)]),
                    field(default=None),
                ),
                ("resource_type", str, field(default=None)),
                ("_create_time", int, field(default=None)),
                ("_create_user", str, field(default=None)),
                ("_last_modified_time", int, field(default=None)),
                ("_last_modified_user", str, field(default=None)),
                ("_protection", str, field(default=None)),
                ("_system_owned", bool, field(default=None)),
                ("description", str, field(default=None)),
                ("display_name", str, field(default=None)),
                ("id", str, field(default=None)),
                (
                    "tags",
                    List[
                        make_dataclass(
                            "tags",
                            [
                                ("scope", str, field(default=None)),
                                ("tag", str, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ] = None,
    marked_for_delete: bool = None,
    overridden: bool = None,
    origin_site_id: str = None,
    owner_id: str = None,
    parent_path: str = None,
    path: str = None,
    realization_id: str = None,
    relative_path: str = None,
    remote_path: str = None,
    unique_id: str = None,
) -> Dict[str, Any]:
    """
    Create or update a segment
        If segment with the segment-id is not already present, create a new segment.
    If it already exists, update the segment with specified attributes.

    Args:
        name(str):
            Idem name of the resource.

        resource_id(str, Optional):
            Segment unique ID. Defaults to None.

        address_bindings(List[dict[str, Any]], Optional):
            Static address binding used for the Segment. This field is deprecated and will be removed in a future release. Please use address_bindings in SegmentPort to configure static bindings. Defaults to None.

            * ip_address (str, Optional):
                IP Address for port binding. Defaults to None.

            * mac_address (str, Optional):
                Mac address for port binding. Defaults to None.

            * vlan_id (int, Optional):
                VLAN ID for port binding. Defaults to None.

        admin_state(str, Optional):
            Admin state represents desired state of segment. It does not reflect the state of other logical entities connected/attached to the segment. Defaults to None.

        advanced_config(dict[str, Any], Optional):
            advanced_config. Defaults to None.

            * address_pool_paths (List[str], Optional):
                Policy path to IP address pools.
                Defaults to None.

            * hybrid (bool, Optional):
                When set to true, all the ports created on this segment will behave
                in a hybrid fashion. The hybrid port indicates to NSX that the
                VM intends to operate in underlay mode, but retains the ability to
                forward egress traffic to the NSX overlay network.
                This property is only applicable for segment created with transport
                zone type OVERLAY_STANDARD.
                This property cannot be modified after segment is created.
                Defaults to None.

            * inter_router (bool, Optional):
                When set to true, any port attached to this logical switch will
                not be visible through VC/ESX UI
                Defaults to None.

            * local_egress (bool, Optional):
                This property is used to enable proximity routing with local egress.
                When set to true, logical router interface (downlink) connecting
                Segment to Tier0/Tier1 gateway is configured with prefix-length 32.
                Defaults to None.

            * local_egress_routing_policies (List[dict[str, Any]], Optional):
                An ordered list of routing policies to forward traffic to the next hop.
                Defaults to None.

                * nexthop_address (str):
                    Next hop address for proximity routing.


                * prefix_list_paths (List[str]):
                    The destination address of traffic matching a prefix-list is forwarded
                    to the nexthop_address. Traffic matching a prefix list with Action
                    DENY will be dropped.
                    Individual prefix-lists specified could have different actions.


            * multicast (bool, Optional):
                Enable multicast on the downlink LRP created to connect the segment to
                Tier0/Tier1 gateway.
                Defaults to None.

            * ndra_profile_path (str, Optional):
                This profile is applie dto the downlink logical router port created
                while attaching this semgnet to tier-0 or tier-1. If this field is
                empty, NDRA profile of the router is applied to the newly created
                port.
                Defaults to None.

            * node_local_switch (bool, Optional):
                A behaviour required for Firewall As A Service (FaaS) where the segment BUM traffic
                is confined within the edge node that this segment belongs to.
                Defaults to None.

            * origin_id (str, Optional):
                ID populated by NSX when NSX on DVPG is used to indicate the source DVPG. Currently, only DVPortgroups are identified as Discovered Segment. The origin_id is the identifier of DVPortgroup from the source vCenter server. Defaults to None.

            * origin_type (str, Optional):
                The type of source from where the DVPortgroup is discovered. Defaults to None.

            * uplink_teaming_policy_name (str, Optional):
                The name of the switching uplink teaming policy for the Segment. This name corresponds to one of the switching uplink teaming policy names listed in TransportZone associated with the Segment. See transport_zone_path property above for more details. When this property is not specified, the segment will not have a teaming policy associated with it and the host switch's default teaming policy will be used by MP. Defaults to None.

            * urpf_mode (str, Optional):
                This URPF mode is applied to the downlink logical router port created
                while attaching this segment to tier-0 or tier-1.
                Defaults to None.

            * connectivity (str, Optional):
                Connectivity configuration to manually connect (ON) or disconnect (OFF)
                Tier-0/Tier1 segment from corresponding gateway.
                This property does not apply to VLAN backed segment. VLAN backed segment
                with connectivity OFF does not affect its layer-2 connectivity.
                Defaults to None.

        bridge_profiles(List[dict[str, Any]], Optional):
            Multiple distinct L2 bridge profiles can be configured. Defaults to None.

            * bridge_profile_path (str):
                Same bridge profile can be configured on different segment. Each bridge profile on a segment must unique.

            * uplink_teaming_policy_name (str, Optional):
                The name of the switching uplink teaming policy for the bridge endpoint. This name corresponds to one fot he switching uplink teaming policy names listed in teh transport zone. When this property is not specified, the teaming policy is assigned by MP. Defaults to None.

            * vlan_ids (List[str], Optional):
                VLAN specification for bridge endpoint. Either VLAN ID or VLAN ranges can be specified. Not both. Defaults to None.

            * vlan_transport_zone_path (str):
                VLAN transport zone should belong to the enforcment-point as the transport zone specified in the segment.

        connectivity_path(str, Optional):
            Policy path to the connecting Tier-0 or Tier-1.
            Valid only for segment created under Infra.
            This field can only be used for overlay segment.
            VLAN backed segment cannot have connectivity path set.
            Defaults to None.

        dhcp_config_path(str, Optional):
            Policy path to DHCP server or relay configuration to use for all
            IPv4 & IPv6 subnets configured on this segment.
            Defaults to None.

        domain_name(str, Optional):
            DNS domain name. Defaults to None.

        evpn_segment(bool, Optional):
            Flag to indicate if the Segment is a Child-Segment of type EVPN. Defaults to None.

        evpn_tenant_config_path(str, Optional):
            Policy path to the EvpnTenantConfig resource. Supported only for Route-Server Evpn Mode.
            Supported only for Overlay segment. This will be populated for both Parent and Child segment
            participating in Evpn Route-Server Mode.
            Defaults to None.

        extra_configs(List[dict[str, Any]], Optional):
            This property could be used for vendor specific configuration in key value
            string pairs, the setting in extra_configs will be automatically inheritted
            by segment ports in the Segment.
            Defaults to None.

            * config_pair (dict[str, Any]):
                config_pair.

                * key (str):
                    Key.

                * value (str):
                    Value.

        federation_config(dict[str, Any], Optional):
            federation_config. Defaults to None.

            * global_overlay_id (int, Optional):
                Global id for by Layer3 services for federation usecases.
                Defaults to None.

        l2_extension(dict[str, Any], Optional):
            l2_extension. Defaults to None.

            * l2vpn_path (str, Optional):
                This property has been deprecated. Please use the property l2vpn_paths
                for setting the paths of associated L2 VPN session. This property will
                continue to work as expected to provide backwards compatibility.
                However, when both l2vpn_path and l2vpn_paths properties
                are specified, only l2vpn_paths is used.
                Defaults to None.

            * l2vpn_paths (List[str], Optional):
                Policy paths corresponding to the associated L2 VPN sessions
                Defaults to None.

            * local_egress (dict[str, Any], Optional):
                local_egress. Defaults to None.

                * optimized_ips (List[str], Optional):
                    Gateway IP for Local Egress. Local egress is enabled only when this
                    list is not empty.
                    Defaults to None.

            * tunnel_id (int, Optional):
                Tunnel ID. Defaults to None.

        ls_id(str, Optional):
            This property is deprecated. The property will continue to work as
            expected for existing segment. The segment that are newly created
            with ls_id will be ignored.
            Sepcify pre-creted logical switch id for Segment.
            Defaults to None.

        mac_pool_id(str, Optional):
            Mac pool id that associated with a Segment. Defaults to None.

        metadata_proxy_paths(List[str], Optional):
            Policy path to metadata proxy configuration. Multiple distinct MD proxies can be configured. Defaults to None.

        overlay_id(int, Optional):
            Used for overlay connectivity of segment. The overlay_id
            should be allocated from the pool as definied by enforcement-point.
            If not provided, it is auto-allocated from the default pool on the
            enforcement-point.
            Defaults to None.

        replication_mode(str, Optional):
            If this field is not set for overlay segment, then the default of MTEP
            will be used.
            Defaults to None.

        subnets(List[dict[str, Any]], Optional):
            Subnet configuration. Max 1 subnet. Defaults to None.

            * dhcp_config (dict[str, Any], Optional):
                dhcp_config. Defaults to None.

                * dns_servers (List[str], Optional):
                    IP address of DNS servers for subnet. DNS server IP address must
                    belong to the same address family as segment gateway_address
                    property.
                    Defaults to None.

                * lease_time (int, Optional):
                    DHCP lease time in seconds. When specified, this property overwrites
                    lease time configured DHCP server config.
                    Defaults to None.

                * resource_type (str):
                    resource_type.

                * server_address (str, Optional):
                    IP address of the DHCP server in CIDR format.
                    The server_address is mandatory in case this segment has provided a
                    dhcp_config_path and it represents a DHCP server config.
                    If this SegmentDhcpConfig is a SegmentDhcpV4Config, the address must
                    be an IPv4 address. If this is a SegmentDhcpV6Config, the address must
                    be an IPv6 address.
                    This address must not overlap the ip-ranges of the subnet, or the
                    gateway address of the subnet, or the DHCP static-binding addresses
                    of this segment.
                    Defaults to None.

            * dhcp_ranges (List[str], Optional):
                DHCP address ranges are used for dynamic IP allocation.
                Supports address range and CIDR formats. First valid
                host address from the first value is assigned to DHCP server
                IP address. Existing values cannot be deleted or modified,
                but additional DHCP ranges can be added.
                Defaults to None.

            * gateway_address (str, Optional):
                Gateway IP address in CIDR format for both IPv4 and IPv6.
                Defaults to None.

            * network (str, Optional):
                Network CIDR for this subnet calculated from gateway_addresses and
                prefix_len.
                Defaults to None.

        transport_zone_path(str, Optional):
            Policy path to the transport zone. Supported for VLAN backed segment
            as well as Overlay segment.
            - This field is required for VLAN backed segment.
            - For overlay segment, it is auto assigned if only one transport zone
              exists in the enforcement point. Default transport zone is auto
              assigned for  overlay segment if none specified.
            Defaults to None.

        type_(str, Optional):
            Segment type based on configuration.
            Defaults to None.

        vlan_ids(List[str], Optional):
            VLAN ids for a VLAN backed Segment.
            Can be a VLAN id or a range of VLAN ids specified with '-' in between.
            Defaults to None.

        children(List[dict[str, Any]], Optional):
            Subtree for this type within policy tree containing nested elements. Note that
            this type is applicable to be used in Hierarchical API only.
            Defaults to None.

            * mark_for_override (bool, Optional):
                Indicates whether this object is the overridden intent object Global intent objects cannot be modified by the user. However, certain global intent objects can be overridden locally by use of this property. In such cases, the overridden local values take precedence over the globally defined values for the properties. Defaults to None.

            * marked_for_delete (bool, Optional):
                If this field is set to true, delete operation is triggered on the
                intent tree. This resource along with its all children in intent tree
                will be deleted. This is a cascade delete and should only be used if
                intent object along with its all children are to be deleted. This does
                not support deletion of single non-leaf node within the tree and should
                be used carefully.
                Defaults to None.

            * request_parameter (dict[str, Any], Optional):
                request_parameter. Defaults to None.

                * resource_type (str):
                    The type of this request parameter.

            * resource_type (str, Optional):
                The type of this resource. Defaults to None.

            * _create_time (int, Optional):
                Timestamp of resource creation. Defaults to None.

            * _create_user (str, Optional):
                ID of the user who created this resource. Defaults to None.

            * _last_modified_time (int, Optional):
                Timestamp of last modification. Defaults to None.

            * _last_modified_user (str, Optional):
                ID of the user who last modified this resource. Defaults to None.

            * _protection (str, Optional):
                Protection status is one of the following:
                PROTECTED - the client who retrieved the entity is not allowed
                            to modify it.
                NOT_PROTECTED - the client who retrieved the entity is allowed
                                to modify it
                REQUIRE_OVERRIDE - the client who retrieved the entity is a super
                                   user and can modify it, but only when providing
                                   the request header X-Allow-Overwrite=true.
                UNKNOWN - the _protection field could not be determined for this
                          entity.
                Defaults to None.

            * _system_owned (bool, Optional):
                Indicates system owned resource. Defaults to None.

            * description (str, Optional):
                Description of this resource. Defaults to None.

            * display_name (str, Optional):
                Defaults to ID if not set. Defaults to None.

            * id (str, Optional):
                Unique identifier of this resource. Defaults to None.

            * tags (List[dict[str, Any]], Optional):
                Opaque identifiers meaningful to the API user. Defaults to None.

                * scope (str, Optional):
                    Tag searches may optionally be restricted by scope. Defaults to None.

                * tag (str, Optional):
                    Identifier meaningful to user with maximum length of 256 characters. Defaults to None.

        marked_for_delete(bool, Optional):
            Intent objects are not directly deleted from the system when a delete
            is invoked on them. They are marked for deletion and only when all the
            realized entities for that intent object gets deleted, the intent object
            is deleted. Objects that are marked for deletion are not returned in
            GET call. One can use the search API to get these objects.
            Defaults to None.

        overridden(bool, Optional):
            Global intent objects cannot be modified by the user.
            However, certain global intent objects can be overridden locally by use
            of this property. In such cases, the overridden local values take
            precedence over the globally defined values for the properties.
            Defaults to None.

        origin_site_id(str, Optional):
            This is a UUID generated by the system for knowing which site owns an object.
            This is used in Pmaas
            Defaults to None.

        owner_id(str, Optional):
            This is a UUID generated by the system for knowing whoes owns this object.
            This is used in Pmaas
            Defaults to None.

        parent_path(str, Optional):
            Path of its parent. Defaults to None.

        path(str, Optional):
            Absolute path of this object. Defaults to None.

        realization_id(str, Optional):
            This is a UUID generated by the system for realizing the entity object.
            In most cases this should be same as 'unique_id' of the entity. However,
            in some cases this can be different because of entities have migrated thier
            unique identifier to NSX Policy intent objects later in the timeline and did
            not use unique_id for realization. Realization id is helpful for users to
            debug data path to correlate the configuration with corresponding intent.
            Defaults to None.

        relative_path(str, Optional):
            Path relative from its parent. Defaults to None.

        remote_path(str, Optional):
            This is the path of the object on the local managers when queried on the PMaaS service,
            and path of the object on PMaaS service when queried from the local managers.
            Defaults to None.

        unique_id(str, Optional):
            This is a UUID generated by the GM/LM to uniquely identify
            entites in a federated environment. For entities that are
            stretched across multiple sites, the same ID will be used
            on all the stretched sites.
            Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_nsx.policy.networking.connectivity.segments.segment_is_present:
              nsx.nsx.policy.networking.connectivity.segments.segment.present:
              - segment_id: string
              - address_bindings:
                - ip_address: string
                  mac_address: string
                  vlan_id: int
              - admin_state: string
              - advanced_config:
                  address_pool_paths:
                  - value
                  connectivity: string
                  hybrid: bool
                  inter_router: bool
                  local_egress: bool
                  local_egress_routing_policies:
                  - nexthop_address: string
                    prefix_list_paths:
                    - value
                  multicast: bool
                  ndra_profile_path: string
                  node_local_switch: bool
                  origin_id: string
                  origin_type: string
                  uplink_teaming_policy_name: string
                  urpf_mode: string
              - bridge_profiles:
                - bridge_profile_path: string
                  uplink_teaming_policy_name: string
                  vlan_ids:
                  - value
                  vlan_transport_zone_path: string
              - connectivity_path: string
              - dhcp_config_path: string
              - domain_name: string
              - evpn_segment: bool
              - evpn_tenant_config_path: string
              - extra_configs:
                - config_pair:
                    key: string
                    value: string
              - federation_config:
                  global_overlay_id: int
              - l2_extension:
                  l2vpn_path: string
                  l2vpn_paths:
                  - value
                  local_egress:
                    optimized_ips:
                    - value
                  tunnel_id: int
              - ls_id: string
              - mac_pool_id: string
              - metadata_proxy_paths:
                - value
              - overlay_id: int
              - replication_mode: string
              - subnets:
                - dhcp_config:
                    dns_servers:
                    - value
                    lease_time: int
                    resource_type: string
                    server_address: string
                  dhcp_ranges:
                  - value
                  gateway_address: string
                  network: string
              - transport_zone_path: string
              - type_: string
              - vlan_ids:
                - value
              - children:
                - _create_time: int
                  _create_user: string
                  _last_modified_time: int
                  _last_modified_user: string
                  _protection: string
                  _system_owned: bool
                  description: string
                  display_name: string
                  id_: string
                  mark_for_override: bool
                  marked_for_delete: bool
                  request_parameter:
                    resource_type: string
                  resource_type: string
                  tags:
                  - scope: string
                    tag: string
              - marked_for_delete: bool
              - overridden: bool
              - origin_site_id: string
              - owner_id: string
              - parent_path: string
              - path: string
              - realization_id: string
              - relative_path: string
              - remote_path: string
              - unique_id: string


    """
    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        before = await hub.exec.nsx.policy.networking.connectivity.segments.segment.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'nsx.segments: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.nsx.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update nsx.segments: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.nsx.policy.networking.connectivity.segments.segment.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "address_bindings": address_bindings,
                        "admin_state": admin_state,
                        "advanced_config": advanced_config,
                        "bridge_profiles": bridge_profiles,
                        "connectivity_path": connectivity_path,
                        "dhcp_config_path": dhcp_config_path,
                        "domain_name": domain_name,
                        "evpn_segment": evpn_segment,
                        "evpn_tenant_config_path": evpn_tenant_config_path,
                        "extra_configs": extra_configs,
                        "federation_config": federation_config,
                        "l2_extension": l2_extension,
                        "ls_id": ls_id,
                        "mac_pool_id": mac_pool_id,
                        "metadata_proxy_paths": metadata_proxy_paths,
                        "overlay_id": overlay_id,
                        "replication_mode": replication_mode,
                        "subnets": subnets,
                        "transport_zone_path": transport_zone_path,
                        "type_": type_,
                        "vlan_ids": vlan_ids,
                        "children": children,
                        "marked_for_delete": marked_for_delete,
                        "overridden": overridden,
                        "origin_site_id": origin_site_id,
                        "owner_id": owner_id,
                        "parent_path": parent_path,
                        "path": path,
                        "realization_id": realization_id,
                        "relative_path": relative_path,
                        "remote_path": remote_path,
                        "unique_id": unique_id,
                        "_revision": before["ret"]["_revision"],
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'nsx.segments: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.nsx.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create nsx.segment: {name}",)
            return result
        else:
            create_ret = await hub.exec.nsx.policy.networking.connectivity.segments.segment.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "address_bindings": address_bindings,
                    "admin_state": admin_state,
                    "advanced_config": advanced_config,
                    "bridge_profiles": bridge_profiles,
                    "connectivity_path": connectivity_path,
                    "dhcp_config_path": dhcp_config_path,
                    "domain_name": domain_name,
                    "evpn_segment": evpn_segment,
                    "evpn_tenant_config_path": evpn_tenant_config_path,
                    "extra_configs": extra_configs,
                    "federation_config": federation_config,
                    "l2_extension": l2_extension,
                    "ls_id": ls_id,
                    "mac_pool_id": mac_pool_id,
                    "metadata_proxy_paths": metadata_proxy_paths,
                    "overlay_id": overlay_id,
                    "replication_mode": replication_mode,
                    "subnets": subnets,
                    "transport_zone_path": transport_zone_path,
                    "type_": type_,
                    "vlan_ids": vlan_ids,
                    "children": children,
                    "marked_for_delete": marked_for_delete,
                    "overridden": overridden,
                    "origin_site_id": origin_site_id,
                    "owner_id": owner_id,
                    "parent_path": parent_path,
                    "path": path,
                    "realization_id": realization_id,
                    "relative_path": relative_path,
                    "remote_path": remote_path,
                    "unique_id": unique_id,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'nsx.segments: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    after = await hub.exec.nsx.policy.networking.connectivity.segments.segment.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(hub, ctx, name: str, resource_id: str = None) -> Dict[str, Any]:
    """

    Delete infra segment
        Delete infra segment

    Args:
        name(str):
            Idem name of the resource.

        resource_id(str, Optional):
            Segment unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_nsx.policy.networking.connectivity.segments.segment_is_absent:
              nsx.nsx.policy.networking.connectivity.segments.segment.absent:
              - segment_id: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(
            f"'nsx.policy.networking.connectivity.segments.segment: {name}' already absent"
        )
        return result

    before = await hub.exec.nsx.policy.networking.connectivity.segments.segment.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result[
                "comment"
            ] = f"Would delete nsx.nsx.policy.networking.connectivity.segments.segment: {name}"
            return result

        delete_ret = (
            await hub.exec.nsx.policy.networking.connectivity.segments.segment.delete(
                ctx,
                name=name,
                resource_id=resource_id,
            )
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(
                f"Deleted nsx.nsx.policy.networking.connectivity.segments.segment: {name}"
            )
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(
            f"nsx.nsx.policy.networking.connectivity.segments.segment: {name} already absent"
        )
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    List all segment under infra
        Paginated list of all segment under infra.

    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe nsx.policy.networking.connectivity.segments.segment
    """

    result = {}

    ret = await hub.exec.nsx.policy.networking.connectivity.segments.segment.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(
            f"Could not describe nsx.policy.networking.connectivity.segments.segment {ret['comment']}"
        )
        return result

    for resource in ret["ret"]:
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "nsx.policy.networking.connectivity.segments.segment.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result
