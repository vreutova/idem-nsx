def __init__(hub):
    hub.exec.nsx.ENDPOINT_URLS = ["https://nsxmanager.your.domain/policy/api/v1"]
    # The default is the first in the list
    hub.exec.nsx.DEFAULT_ENDPOINT_URL = "https://nsxmanager.your.domain/policy/api/v1"

    # This enables acct profiles that begin with "nsx" for nsx modules
    hub.exec.nsx.ACCT = ["nsx"]
