"""Exec module for managing Policy Networking Connectivity Segments Ports. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(
    hub, ctx, resource_id: str, segment_id: str, name: str = None
) -> Dict[str, Any]:
    """
    Get infra segment port by ID
        Get detail information on an infra segment port by giving ID.

    Args:
        resource_id(str):
            Port ID.

        segment_id(str):
            Segment ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: nsx.policy.networking.connectivity.segments.port.get
                - kwargs:
                  resource_id: value
                  segment_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec nsx.policy.networking.connectivity.segments.port.get resource_id=value, segment_id=value
    """

    result = dict(comment=[], ret=None, result=True)

    get = await hub.tool.nsx.session.request(
        ctx,
        method="get",
        path="/infra/segments/{segment-id}/ports/{port-id}".format(
            **{"segment-id": segment_id, "port-id": resource_id}
        ),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    resource_in_present_format = {
        "name": name,
        "resource_id": resource_id,
        "segment_id": segment_id,
    }
    resource_parameters = OrderedDict(
        {
            "address_bindings": "address_bindings",
            "admin_state": "admin_state",
            "attachment": "attachment",
            "children": "children",
            "extra_configs": "extra_configs",
            "ignored_address_bindings": "ignored_address_bindings",
            "init_state": "init_state",
            "marked_for_delete": "marked_for_delete",
            "origin_id": "origin_id",
            "origin_site_id": "origin_site_id",
            "overridden": "overridden",
            "owner_id": "owner_id",
            "parent_path": "parent_path",
            "path": "path",
            "realization_id": "realization_id",
            "relative_path": "relative_path",
            "remote_path": "remote_path",
            "source_site_id": "source_site_id",
            "unique_id": "unique_id",
            "_revision": "_revision",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource:
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(hub, ctx, segment_id: str) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: nsx.policy.networking.connectivity.segments.port.list
                - kwargs:
                  segment_id: value


        Exec call from the CLI:

        .. code-block:: bash

            idem exec nsx.policy.networking.connectivity.segments.port.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe nsx.policy.networking.connectivity.segments.port

    """

    result = dict(comment=[], ret=[], result=True)

    list = await hub.tool.nsx.session.request(
        ctx,
        method="get",
        path="/infra/segments/{segment-id}/ports".format(**{"segment-id": segment_id}),
        query_params={},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]["results"]:

        # Convert raw response into present format
        resource_in_present_format = {"segment_id": segment_id}
        resource_parameters = OrderedDict(
            {
                "id": "resource_id",
                "address_bindings": "address_bindings",
                "admin_state": "admin_state",
                "attachment": "attachment",
                "children": "children",
                "extra_configs": "extra_configs",
                "ignored_address_bindings": "ignored_address_bindings",
                "init_state": "init_state",
                "marked_for_delete": "marked_for_delete",
                "origin_id": "origin_id",
                "origin_site_id": "origin_site_id",
                "overridden": "overridden",
                "owner_id": "owner_id",
                "parent_path": "parent_path",
                "path": "path",
                "realization_id": "realization_id",
                "relative_path": "relative_path",
                "remote_path": "remote_path",
                "source_site_id": "source_site_id",
                "unique_id": "unique_id",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    segment_id: str,
    resource_id: str = None,
    name: str = None,
    address_bindings: List[
        make_dataclass(
            "address_bindings",
            [
                ("ip_address", str, field(default=None)),
                ("mac_address", str, field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ] = None,
    admin_state: str = None,
    attachment: make_dataclass(
        "attachment",
        [
            ("allocate_addresses", str, field(default=None)),
            ("app_id", str, field(default=None)),
            (
                "bms_interface_config",
                make_dataclass(
                    "bms_interface_config",
                    [
                        ("app_intf_name", str),
                        ("default_gateway", str, field(default=None)),
                        ("migrate_intf", str, field(default=None)),
                        ("routing_table", List[str], field(default=None)),
                    ],
                ),
                field(default=None),
            ),
            ("context_id", str, field(default=None)),
            ("context_type", str, field(default=None)),
            ("evpn_vlans", List[str], field(default=None)),
            ("hyperbus_mode", str, field(default=None)),
            ("id", str, field(default=None)),
            ("traffic_tag", int, field(default=None)),
            ("type", str, field(default=None)),
        ],
    ) = None,
    extra_configs: List[
        make_dataclass(
            "extra_configs",
            [
                (
                    "config_pair",
                    make_dataclass("config_pair", [("key", str), ("value", str)]),
                )
            ],
        )
    ] = None,
    ignored_address_bindings: List[
        make_dataclass(
            "ignored_address_bindings",
            [
                ("ip_address", str, field(default=None)),
                ("mac_address", str, field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ] = None,
    init_state: str = None,
    origin_id: str = None,
    source_site_id: str = None,
    children: List[
        make_dataclass(
            "children",
            [
                ("mark_for_override", bool, field(default=None)),
                ("marked_for_delete", bool, field(default=None)),
                (
                    "request_parameter",
                    make_dataclass("request_parameter", [("resource_type", str)]),
                    field(default=None),
                ),
                ("resource_type", str, field(default=None)),
                ("_create_time", int, field(default=None)),
                ("_create_user", str, field(default=None)),
                ("_last_modified_time", int, field(default=None)),
                ("_last_modified_user", str, field(default=None)),
                ("_protection", str, field(default=None)),
                ("_system_owned", bool, field(default=None)),
                ("description", str, field(default=None)),
                ("display_name", str, field(default=None)),
                ("id", str, field(default=None)),
                (
                    "tags",
                    List[
                        make_dataclass(
                            "tags",
                            [
                                ("scope", str, field(default=None)),
                                ("tag", str, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ] = None,
    marked_for_delete: bool = None,
    overridden: bool = None,
    origin_site_id: str = None,
    owner_id: str = None,
    parent_path: str = None,
    path: str = None,
    realization_id: str = None,
    relative_path: str = None,
    remote_path: str = None,
    unique_id: str = None,
) -> Dict[str, Any]:
    """
    Patch an infra segment port
        Create an infra segment port if it does not exist based on the IDs, or update existing port
    information by replacing the port object fields which presents in the request body.

    Args:
        segment_id(str):
            None.

        resource_id(str, Optional):
            Port unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        address_bindings(List[dict[str, Any]], Optional):
            Static address binding used for the port. Defaults to None.

            * ip_address (str, Optional):
                IP Address for port binding. Defaults to None.

            * mac_address (str, Optional):
                Mac address for port binding. Defaults to None.

            * vlan_id (int, Optional):
                VLAN ID for port binding. Defaults to None.

        admin_state(str, Optional):
            Represents desired state of the segment port. Defaults to None.

        attachment(dict[str, Any], Optional):
            attachment. Defaults to None.

            * allocate_addresses (str, Optional):
                Indicate how IP will be allocated for the port. Defaults to None.

            * app_id (str, Optional):
                ID used to identify/look up a child attachment behind a parent attachment
                Defaults to None.

            * bms_interface_config (dict[str, Any], Optional):
                bms_interface_config. Defaults to None.

                * app_intf_name (str):
                    The name of application interface.

                * default_gateway (str, Optional):
                    Gateway IP. Defaults to None.

                * migrate_intf (str, Optional):
                    IP configuration on migrate_intf will migrate to app_intf_name. It is used for Management and Application sharing the same IP. Defaults to None.

                * routing_table (List[str], Optional):
                    Routing rules. Defaults to None.

            * context_id (str, Optional):
                If type is CHILD and the parent port is on the same segment as the child port, then this
                field should be VIF ID of the parent port.
                If type is CHILD and the parent port is on a different segment, then this
                field should be policy path of the parent port.
                If type is INDEPENDENT/STATIC, then this field should be transport node ID.
                Defaults to None.

            * context_type (str, Optional):
                Set to PARENT when type field is CHILD. Read only field. Defaults to None.

            * evpn_vlans (List[str], Optional):
                List of Evpn tenant VLAN IDs the Parent logical-port serves in Evpn Route-Server mode. Only effective when attachment type is PARENT and the logical-port is attached to vRouter VM. Defaults to None.

            * hyperbus_mode (str, Optional):
                Flag to indicate if hyperbus configuration is required. Defaults to None.

            * id (str, Optional):
                VIF UUID on NSX Manager. If the attachement type is PARENT, this property is required. Defaults to None.

            * traffic_tag (int, Optional):
                Not valid when type field is INDEPENDENT, mainly used to identify
                traffic from different port in container use case.
                Defaults to None.

            * type (str, Optional):
                Type of port attachment. STATIC is added to replace INDEPENDENT. INDEPENDENT type and PARENT type are deprecated. Defaults to None.

        extra_configs(List[dict[str, Any]], Optional):
            This property could be used for vendor specific configuration in key value
            string pairs. Segment port setting will override segment setting if
            the same key was set on both segment and segment port.
            Defaults to None.

            * config_pair (dict[str, Any]):
                config_pair.

                * key (str):
                    Key.

                * value (str):
                    Value.

        ignored_address_bindings(List[dict[str, Any]], Optional):
            IP Discovery module uses various mechanisms to discover address
            bindings being used on each segment port. If a user would like to
            ignore any specific discovered address bindings or prevent the
            discovery of a particular set of discovered bindings, then those
            address bindings can be provided here. Currently IP range in CIDR format
            is not supported.
            Defaults to None.

            * ip_address (str, Optional):
                IP Address for port binding. Defaults to None.

            * mac_address (str, Optional):
                Mac address for port binding. Defaults to None.

            * vlan_id (int, Optional):
                VLAN ID for port binding. Defaults to None.

        init_state(str, Optional):
            Set initial state when a new logical port is created. 'UNBLOCKED_VLAN'
            means new port will be unblocked on traffic in creation, also VLAN will
            be set with corresponding logical switch setting. This port setting
            can only be configured at port creation, and cannot be modified.
            'RESTORE_VIF' fetches and restores VIF attachment from ESX host.
            Defaults to None.

        origin_id(str, Optional):
            ID populated by NSX when NSX on DVPG is used to indicate the source Distributed Virtual Port
            and the corresponding Distributed Virtual Switch. This ID is populated only for port attached to
            discovered segments.
            Defaults to None.

        source_site_id(str, Optional):
            This field will refer to the source site on which the segment
            port is discovered. This field is populated by GM, when it
            receives corresponding notification from LM.
            Defaults to None.

        children(List[dict[str, Any]], Optional):
            Subtree for this type within policy tree containing nested elements. Note that
            this type is applicable to be used in Hierarchical API only.
            Defaults to None.

            * mark_for_override (bool, Optional):
                Indicates whether this object is the overridden intent object Global intent objects cannot be modified by the user. However, certain global intent objects can be overridden locally by use of this property. In such cases, the overridden local values take precedence over the globally defined values for the properties. Defaults to None.

            * marked_for_delete (bool, Optional):
                If this field is set to true, delete operation is triggered on the
                intent tree. This resource along with its all children in intent tree
                will be deleted. This is a cascade delete and should only be used if
                intent object along with its all children are to be deleted. This does
                not support deletion of single non-leaf node within the tree and should
                be used carefully.
                Defaults to None.

            * request_parameter (dict[str, Any], Optional):
                request_parameter. Defaults to None.

                * resource_type (str):
                    The type of this request parameter.

            * resource_type (str, Optional):
                The type of this resource. Defaults to None.

            * _create_time (int, Optional):
                Timestamp of resource creation. Defaults to None.

            * _create_user (str, Optional):
                ID of the user who created this resource. Defaults to None.

            * _last_modified_time (int, Optional):
                Timestamp of last modification. Defaults to None.

            * _last_modified_user (str, Optional):
                ID of the user who last modified this resource. Defaults to None.

            * _protection (str, Optional):
                Protection status is one of the following:
                PROTECTED - the client who retrieved the entity is not allowed
                            to modify it.
                NOT_PROTECTED - the client who retrieved the entity is allowed
                                to modify it
                REQUIRE_OVERRIDE - the client who retrieved the entity is a super
                                   user and can modify it, but only when providing
                                   the request header X-Allow-Overwrite=true.
                UNKNOWN - the _protection field could not be determined for this
                          entity.
                Defaults to None.

            * _system_owned (bool, Optional):
                Indicates system owned resource. Defaults to None.

            * description (str, Optional):
                Description of this resource. Defaults to None.

            * display_name (str, Optional):
                Defaults to ID if not set. Defaults to None.

            * id (str, Optional):
                Unique identifier of this resource. Defaults to None.

            * tags (List[dict[str, Any]], Optional):
                Opaque identifiers meaningful to the API user. Defaults to None.

                * scope (str, Optional):
                    Tag searches may optionally be restricted by scope. Defaults to None.

                * tag (str, Optional):
                    Identifier meaningful to user with maximum length of 256 characters. Defaults to None.

        marked_for_delete(bool, Optional):
            Intent objects are not directly deleted from the system when a delete
            is invoked on them. They are marked for deletion and only when all the
            realized entities for that intent object gets deleted, the intent object
            is deleted. Objects that are marked for deletion are not returned in
            GET call. One can use the search API to get these objects.
            Defaults to None.

        overridden(bool, Optional):
            Global intent objects cannot be modified by the user.
            However, certain global intent objects can be overridden locally by use
            of this property. In such cases, the overridden local values take
            precedence over the globally defined values for the properties.
            Defaults to None.

        origin_site_id(str, Optional):
            This is a UUID generated by the system for knowing which site owns an object.
            This is used in Pmaas
            Defaults to None.

        owner_id(str, Optional):
            This is a UUID generated by the system for knowing whoes owns this object.
            This is used in Pmaas
            Defaults to None.

        parent_path(str, Optional):
            Path of its parent. Defaults to None.

        path(str, Optional):
            Absolute path of this object. Defaults to None.

        realization_id(str, Optional):
            This is a UUID generated by the system for realizing the entity object.
            In most cases this should be same as 'unique_id' of the entity. However,
            in some cases this can be different because of entities have migrated thier
            unique identifier to NSX Policy intent objects later in the timeline and did
            not use unique_id for realization. Realization id is helpful for users to
            debug data path to correlate the configuration with corresponding intent.
            Defaults to None.

        relative_path(str, Optional):
            Path relative from its parent. Defaults to None.

        remote_path(str, Optional):
            This is the path of the object on the local managers when queried on the PMaaS service,
            and path of the object on PMaaS service when queried from the local managers.
            Defaults to None.

        unique_id(str, Optional):
            This is a UUID generated by the GM/LM to uniquely identify
            entites in a federated environment. For entities that are
            stretched across multiple sites, the same ID will be used
            on all the stretched sites.
            Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              nsx.policy.networking.connectivity.segments.port.present:
                - segment_id: value
                - port_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec nsx.policy.networking.connectivity.segments.port.create segment_id=value, port_id=value
    """
    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    resource_to_raw_input_mapping = {
        "address_bindings": "address_bindings",
        "admin_state": "admin_state",
        "attachment": "attachment",
        "extra_configs": "extra_configs",
        "ignored_address_bindings": "ignored_address_bindings",
        "init_state": "init_state",
        "origin_id": "origin_id",
        "source_site_id": "source_site_id",
        "children": "children",
        "marked_for_delete": "marked_for_delete",
        "overridden": "overridden",
        "origin_site_id": "origin_site_id",
        "owner_id": "owner_id",
        "parent_path": "parent_path",
        "path": "path",
        "realization_id": "realization_id",
        "relative_path": "relative_path",
        "remote_path": "remote_path",
        "unique_id": "unique_id",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.nsx.session.request(
        ctx,
        method="put",
        path="/infra/segments/{segment-id}/ports/{port-id}".format(
            **{"segment-id": segment_id, "port-id": resource_id or name}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created nsx.port '{name}'",
    )

    result["ret"] = create["ret"]
    result["ret"]["resource_id"] = resource_id or create["ret"]["id"]
    result["ret"]["segment_id"] = segment_id
    return result


async def update(
    hub,
    ctx,
    resource_id: str,
    segment_id: str,
    name: str = None,
    address_bindings: List[
        make_dataclass(
            "address_bindings",
            [
                ("ip_address", str, field(default=None)),
                ("mac_address", str, field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ] = None,
    admin_state: str = None,
    attachment: make_dataclass(
        "attachment",
        [
            ("allocate_addresses", str, field(default=None)),
            ("app_id", str, field(default=None)),
            (
                "bms_interface_config",
                make_dataclass(
                    "bms_interface_config",
                    [
                        ("app_intf_name", str),
                        ("default_gateway", str, field(default=None)),
                        ("migrate_intf", str, field(default=None)),
                        ("routing_table", List[str], field(default=None)),
                    ],
                ),
                field(default=None),
            ),
            ("context_id", str, field(default=None)),
            ("context_type", str, field(default=None)),
            ("evpn_vlans", List[str], field(default=None)),
            ("hyperbus_mode", str, field(default=None)),
            ("id", str, field(default=None)),
            ("traffic_tag", int, field(default=None)),
            ("type", str, field(default=None)),
        ],
    ) = None,
    extra_configs: List[
        make_dataclass(
            "extra_configs",
            [
                (
                    "config_pair",
                    make_dataclass("config_pair", [("key", str), ("value", str)]),
                )
            ],
        )
    ] = None,
    ignored_address_bindings: List[
        make_dataclass(
            "ignored_address_bindings",
            [
                ("ip_address", str, field(default=None)),
                ("mac_address", str, field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ] = None,
    init_state: str = None,
    origin_id: str = None,
    source_site_id: str = None,
    children: List[
        make_dataclass(
            "children",
            [
                ("mark_for_override", bool, field(default=None)),
                ("marked_for_delete", bool, field(default=None)),
                (
                    "request_parameter",
                    make_dataclass("request_parameter", [("resource_type", str)]),
                    field(default=None),
                ),
                ("resource_type", str, field(default=None)),
                ("_create_time", int, field(default=None)),
                ("_create_user", str, field(default=None)),
                ("_last_modified_time", int, field(default=None)),
                ("_last_modified_user", str, field(default=None)),
                ("_protection", str, field(default=None)),
                ("_system_owned", bool, field(default=None)),
                ("description", str, field(default=None)),
                ("display_name", str, field(default=None)),
                ("id", str, field(default=None)),
                (
                    "tags",
                    List[
                        make_dataclass(
                            "tags",
                            [
                                ("scope", str, field(default=None)),
                                ("tag", str, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ] = None,
    marked_for_delete: bool = None,
    overridden: bool = None,
    origin_site_id: str = None,
    owner_id: str = None,
    parent_path: str = None,
    path: str = None,
    realization_id: str = None,
    relative_path: str = None,
    remote_path: str = None,
    _revision: str = None,
    unique_id: str = None,
) -> Dict[str, Any]:
    """
    Patch an infra segment port
        Create an infra segment port if it does not exist based on the IDs, or update existing port
    information by replacing the port object fields which presents in the request body.

    Args:
        resource_id(str):
            Port unique ID.

        segment_id(str):
            None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        address_bindings(List[dict[str, Any]], Optional):
            Static address binding used for the port. Defaults to None.

            * ip_address (str, Optional):
                IP Address for port binding. Defaults to None.

            * mac_address (str, Optional):
                Mac address for port binding. Defaults to None.

            * vlan_id (int, Optional):
                VLAN ID for port binding. Defaults to None.

        admin_state(str, Optional):
            Represents desired state of the segment port. Defaults to None.

        attachment(dict[str, Any], Optional):
            attachment. Defaults to None.

            * allocate_addresses (str, Optional):
                Indicate how IP will be allocated for the port. Defaults to None.

            * app_id (str, Optional):
                ID used to identify/look up a child attachment behind a parent attachment
                Defaults to None.

            * bms_interface_config (dict[str, Any], Optional):
                bms_interface_config. Defaults to None.

                * app_intf_name (str):
                    The name of application interface.

                * default_gateway (str, Optional):
                    Gateway IP. Defaults to None.

                * migrate_intf (str, Optional):
                    IP configuration on migrate_intf will migrate to app_intf_name. It is used for Management and Application sharing the same IP. Defaults to None.

                * routing_table (List[str], Optional):
                    Routing rules. Defaults to None.

            * context_id (str, Optional):
                If type is CHILD and the parent port is on the same segment as the child port, then this
                field should be VIF ID of the parent port.
                If type is CHILD and the parent port is on a different segment, then this
                field should be policy path of the parent port.
                If type is INDEPENDENT/STATIC, then this field should be transport node ID.
                Defaults to None.

            * context_type (str, Optional):
                Set to PARENT when type field is CHILD. Read only field. Defaults to None.

            * evpn_vlans (List[str], Optional):
                List of Evpn tenant VLAN IDs the Parent logical-port serves in Evpn Route-Server mode. Only effective when attachment type is PARENT and the logical-port is attached to vRouter VM. Defaults to None.

            * hyperbus_mode (str, Optional):
                Flag to indicate if hyperbus configuration is required. Defaults to None.

            * id (str, Optional):
                VIF UUID on NSX Manager. If the attachement type is PARENT, this property is required. Defaults to None.

            * traffic_tag (int, Optional):
                Not valid when type field is INDEPENDENT, mainly used to identify
                traffic from different port in container use case.
                Defaults to None.

            * type (str, Optional):
                Type of port attachment. STATIC is added to replace INDEPENDENT. INDEPENDENT type and PARENT type are deprecated. Defaults to None.

        extra_configs(List[dict[str, Any]], Optional):
            This property could be used for vendor specific configuration in key value
            string pairs. Segment port setting will override segment setting if
            the same key was set on both segment and segment port.
            Defaults to None.

            * config_pair (dict[str, Any]):
                config_pair.

                * key (str):
                    Key.

                * value (str):
                    Value.

        ignored_address_bindings(List[dict[str, Any]], Optional):
            IP Discovery module uses various mechanisms to discover address
            bindings being used on each segment port. If a user would like to
            ignore any specific discovered address bindings or prevent the
            discovery of a particular set of discovered bindings, then those
            address bindings can be provided here. Currently IP range in CIDR format
            is not supported.
            Defaults to None.

            * ip_address (str, Optional):
                IP Address for port binding. Defaults to None.

            * mac_address (str, Optional):
                Mac address for port binding. Defaults to None.

            * vlan_id (int, Optional):
                VLAN ID for port binding. Defaults to None.

        init_state(str, Optional):
            Set initial state when a new logical port is created. 'UNBLOCKED_VLAN'
            means new port will be unblocked on traffic in creation, also VLAN will
            be set with corresponding logical switch setting. This port setting
            can only be configured at port creation, and cannot be modified.
            'RESTORE_VIF' fetches and restores VIF attachment from ESX host.
            Defaults to None.

        origin_id(str, Optional):
            ID populated by NSX when NSX on DVPG is used to indicate the source Distributed Virtual Port
            and the corresponding Distributed Virtual Switch. This ID is populated only for port attached to
            discovered segments.
            Defaults to None.

        source_site_id(str, Optional):
            This field will refer to the source site on which the segment
            port is discovered. This field is populated by GM, when it
            receives corresponding notification from LM.
            Defaults to None.

        children(List[dict[str, Any]], Optional):
            Subtree for this type within policy tree containing nested elements. Note that
            this type is applicable to be used in Hierarchical API only.
            Defaults to None.

            * mark_for_override (bool, Optional):
                Indicates whether this object is the overridden intent object Global intent objects cannot be modified by the user. However, certain global intent objects can be overridden locally by use of this property. In such cases, the overridden local values take precedence over the globally defined values for the properties. Defaults to None.

            * marked_for_delete (bool, Optional):
                If this field is set to true, delete operation is triggered on the
                intent tree. This resource along with its all children in intent tree
                will be deleted. This is a cascade delete and should only be used if
                intent object along with its all children are to be deleted. This does
                not support deletion of single non-leaf node within the tree and should
                be used carefully.
                Defaults to None.

            * request_parameter (dict[str, Any], Optional):
                request_parameter. Defaults to None.

                * resource_type (str):
                    The type of this request parameter.

            * resource_type (str, Optional):
                The type of this resource. Defaults to None.

            * _create_time (int, Optional):
                Timestamp of resource creation. Defaults to None.

            * _create_user (str, Optional):
                ID of the user who created this resource. Defaults to None.

            * _last_modified_time (int, Optional):
                Timestamp of last modification. Defaults to None.

            * _last_modified_user (str, Optional):
                ID of the user who last modified this resource. Defaults to None.

            * _protection (str, Optional):
                Protection status is one of the following:
                PROTECTED - the client who retrieved the entity is not allowed
                            to modify it.
                NOT_PROTECTED - the client who retrieved the entity is allowed
                                to modify it
                REQUIRE_OVERRIDE - the client who retrieved the entity is a super
                                   user and can modify it, but only when providing
                                   the request header X-Allow-Overwrite=true.
                UNKNOWN - the _protection field could not be determined for this
                          entity.
                Defaults to None.

            * _system_owned (bool, Optional):
                Indicates system owned resource. Defaults to None.

            * description (str, Optional):
                Description of this resource. Defaults to None.

            * display_name (str, Optional):
                Defaults to ID if not set. Defaults to None.

            * id (str, Optional):
                Unique identifier of this resource. Defaults to None.

            * tags (List[dict[str, Any]], Optional):
                Opaque identifiers meaningful to the API user. Defaults to None.

                * scope (str, Optional):
                    Tag searches may optionally be restricted by scope. Defaults to None.

                * tag (str, Optional):
                    Identifier meaningful to user with maximum length of 256 characters. Defaults to None.

        marked_for_delete(bool, Optional):
            Intent objects are not directly deleted from the system when a delete
            is invoked on them. They are marked for deletion and only when all the
            realized entities for that intent object gets deleted, the intent object
            is deleted. Objects that are marked for deletion are not returned in
            GET call. One can use the search API to get these objects.
            Defaults to None.

        overridden(bool, Optional):
            Global intent objects cannot be modified by the user.
            However, certain global intent objects can be overridden locally by use
            of this property. In such cases, the overridden local values take
            precedence over the globally defined values for the properties.
            Defaults to None.

        origin_site_id(str, Optional):
            This is a UUID generated by the system for knowing which site owns an object.
            This is used in Pmaas
            Defaults to None.

        owner_id(str, Optional):
            This is a UUID generated by the system for knowing whoes owns this object.
            This is used in Pmaas
            Defaults to None.

        parent_path(str, Optional):
            Path of its parent. Defaults to None.

        path(str, Optional):
            Absolute path of this object. Defaults to None.

        realization_id(str, Optional):
            This is a UUID generated by the system for realizing the entity object.
            In most cases this should be same as 'unique_id' of the entity. However,
            in some cases this can be different because of entities have migrated thier
            unique identifier to NSX Policy intent objects later in the timeline and did
            not use unique_id for realization. Realization id is helpful for users to
            debug data path to correlate the configuration with corresponding intent.
            Defaults to None.

        relative_path(str, Optional):
            Path relative from its parent. Defaults to None.

        remote_path(str, Optional):
            This is the path of the object on the local managers when queried on the PMaaS service,
            and path of the object on PMaaS service when queried from the local managers.
            Defaults to None.

        unique_id(str, Optional):
            This is a UUID generated by the GM/LM to uniquely identify
            entites in a federated environment. For entities that are
            stretched across multiple sites, the same ID will be used
            on all the stretched sites.
            Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              nsx.policy.networking.connectivity.segments.port.present:
                - resource_id: value
                - segment_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec nsx.policy.networking.connectivity.segments.port.update resource_id=value, segment_id=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    resource_to_raw_input_mapping = {
        "_revision": _revision,
        "address_bindings": "address_bindings",
        "admin_state": "admin_state",
        "attachment": "attachment",
        "extra_configs": "extra_configs",
        "ignored_address_bindings": "ignored_address_bindings",
        "init_state": "init_state",
        "origin_id": "origin_id",
        "source_site_id": "source_site_id",
        "children": "children",
        "marked_for_delete": "marked_for_delete",
        "overridden": "overridden",
        "origin_site_id": "origin_site_id",
        "owner_id": "owner_id",
        "parent_path": "parent_path",
        "path": "path",
        "realization_id": "realization_id",
        "relative_path": "relative_path",
        "remote_path": "remote_path",
        "unique_id": "unique_id",
        "_revision": "_revision",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.nsx.session.request(
            ctx,
            method="put",
            path="/infra/segments/{segment-id}/ports/{port-id}".format(
                **{"segment-id": segment_id, "port-id": resource_id}
            ),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated nsx.port '{name}'",
        )

    return result


async def delete(
    hub, ctx, resource_id: str, segment_id: str, name: str = None
) -> Dict[str, Any]:
    """
    Delete an infra segment port
        Delete an infra segment port by giving ID.

    Args:
        resource_id(str):
            Port unique ID.

        segment_id(str):
            Segment unique ID..

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              nsx.policy.networking.connectivity.segments.port.absent:
                - resource_id: value
                - segment_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec nsx.policy.networking.connectivity.segments.port.delete resource_id=value, segment_id=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.nsx.session.request(
        ctx,
        method="delete",
        path="/infra/segments/{segment-id}/ports/{port-id}".format(
            **{"segment-id": segment_id, "port-id": resource_id}
        ),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result
