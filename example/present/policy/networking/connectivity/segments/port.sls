idem_test_policy.networking.connectivity.segments.port_is_present:
  nsx.policy.networking.connectivity.segments.port.present:
  - segment_id: string
  - resource_id: string
  - address_bindings:
    - ip_address: string
      mac_address: string
      vlan_id: int
  - admin_state: string
  - attachment:
      allocate_addresses: string
      app_id: string
      bms_interface_config:
        app_intf_name: string
        default_gateway: string
        migrate_intf: string
        routing_table:
        - value
      context_id: string
      context_type: string
      evpn_vlans:
      - value
      hyperbus_mode: string
      id_: string
      traffic_tag: int
      type_: string
  - extra_configs:
    - config_pair:
        key: string
        value: string
  - ignored_address_bindings:
    - ip_address: string
      mac_address: string
      vlan_id: int
  - init_state: string
  - origin_id: string
  - source_site_id: string
  - children:
    - _create_time: int
      _create_user: string
      _last_modified_time: int
      _last_modified_user: string
      _protection: string
      _system_owned: bool
      description: string
      display_name: string
      id_: string
      mark_for_override: bool
      marked_for_delete: bool
      request_parameter:
        resource_type: string
      resource_type: string
      tags:
      - scope: string
        tag: string
  - marked_for_delete: bool
  - overridden: bool
  - origin_site_id: string
  - owner_id: string
  - parent_path: string
  - path: string
  - realization_id: string
  - relative_path: string
  - remote_path: string
  - unique_id: string
