idem_test_policy.networking.connectivity.segments.segments_is_present:
  nsx.policy.networking.connectivity.segments.segment.present:
  - resource_id: string
  - address_bindings:
    - ip_address: string
      mac_address: string
      vlan_id: int
  - admin_state: string
  - advanced_config:
      address_pool_paths:
      - value
      connectivity: string
      hybrid: bool
      inter_router: bool
      local_egress: bool
      local_egress_routing_policies:
      - nexthop_address: string
        prefix_list_paths:
        - value
      multicast: bool
      ndra_profile_path: string
      node_local_switch: bool
      origin_id: string
      origin_type: string
      uplink_teaming_policy_name: string
      urpf_mode: string
  - bridge_profiles:
    - bridge_profile_path: string
      uplink_teaming_policy_name: string
      vlan_ids:
      - value
      vlan_transport_zone_path: string
  - connectivity_path: string
  - dhcp_config_path: string
  - domain_name: string
  - evpn_segment: bool
  - evpn_tenant_config_path: string
  - extra_configs:
    - config_pair:
        key: string
        value: string
  - federation_config:
      global_overlay_id: int
  - l2_extension:
      l2vpn_path: string
      l2vpn_paths:
      - value
      local_egress:
        optimized_ips:
        - value
      tunnel_id: int
  - ls_id: string
  - mac_pool_id: string
  - metadata_proxy_paths:
    - value
  - overlay_id: int
  - replication_mode: string
  - subnets:
    - dhcp_config:
        dns_servers:
        - value
        lease_time: int
        resource_type: string
        server_address: string
      dhcp_ranges:
      - value
      gateway_address: string
      network: string
  - transport_zone_path: string
  - type_: string
  - vlan_ids:
    - value
  - children:
    - _create_time: int
      _create_user: string
      _last_modified_time: int
      _last_modified_user: string
      _protection: string
      _system_owned: bool
      description: string
      display_name: string
      id_: string
      mark_for_override: bool
      marked_for_delete: bool
      request_parameter:
        resource_type: string
      resource_type: string
      tags:
      - scope: string
        tag: string
  - marked_for_delete: bool
  - overridden: bool
  - origin_site_id: string
  - owner_id: string
  - parent_path: string
  - path: string
  - realization_id: string
  - relative_path: string
  - remote_path: string
  - unique_id: string
